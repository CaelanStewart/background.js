/**
 * Function.prototype.bind polyfill, compliments of Mozilla Developer Network
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
 */

if (!Function.prototype.bind) {
	Function.prototype.bind = function(oThis) {
		if (typeof this !== 'function') {
			// closest thing possible to the ECMAScript 5
			// internal IsCallable function
			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
		}

		var aArgs   = Array.prototype.slice.call(arguments, 1),
			fToBind = this,
			fNOP    = function() {},
			fBound  = function() {
				return fToBind.apply(this instanceof fNOP
						? this
						: oThis,
					aArgs.concat(Array.prototype.slice.call(arguments)));
			};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();

		return fBound;
	};
}

/**
 * Background class, used to replicate the behaviour of the CSS "background-size" property.
 * @type {{init: Function, setElements: Function, setEvents: Function, onResize: Function, getInfo: Function, setInfo: Function, backgroundCover: Function, backgroundContain: Function}}
 * @license
 
 The MIT License (MIT)

 Copyright (c) 2015 Caelan Stewart

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

var Background = {
	/**
	 * Init function
	 * @param selector string|function|undefined If not supplied, the default selector ".background" is use. A function
	 * can be passed to define the selector at run time.
	 * @param events boolean Should this script set its own events? Assumes true by default.
	 */
	init: function(selector, events) {
		"use strict";
		
		if(typeof selector === "string") {
			this.selector = selector;
		} else if(typeof selector === "function") {
			this.selector = selector(); // in case the user wishes to define the selector at run-time
		} else {
			this.selector = ".background"; // default selector
		}
		
		this.setElements();
		
		if(events !== false) {
			this.setEvents();
		}
		
		this.resizeTimeout = false;
		this.onResize();
	},

	/**
	 * Gets the required information to make calculations to size the background
	 * @param node
	 * @returns object
	 */
	getInfo: function(node) {
		node.size.parentWidth    = node.node.parentNode.offsetWidth;
		node.size.parentHeight   = node.node.parentNode.offsetHeight;
		
		return node;
	},

	/**
	 * Sets new, calculated size information to "node"
	 * @param node object
	 * @param info object
	 */
	setInfo: function(node, info) {
		node = node.node;
		
		node.style.width    = info.width + 'px';
		node.style.height   = info.height + 'px';
		node.style.left     = info.left + 'px';
		node.style.top      = info.top + 'px';
	},

	/**
	 * Selects all elements with a selector matching "this.selector"
	 */
	setElements: function() {
		"use strict";
		
		var nodeList = document.querySelectorAll(this.selector),
			i,
			node,
			newNode,
			type;
			
		this.backgrounds = [ ];
		
		/* we want to cache as much info as possible to speed up the script. */
		for(i = 0; node = nodeList[i]; i++) {
			type = node.getAttribute('data-type');
			if(type !== "cover" && type !== "contain") {
				type = "cover"; // default type
			}
			
			newNode = {
				size: {
					width:  parseInt(node.getAttribute('width'), 10),
					height: parseInt(node.getAttribute('height'), 10)
				},
				node: node,
				type: type
			};
			
			this.backgrounds.push(newNode);
		}
	},

	/**
	 * Sets required events
	 */
	setEvents: function() {
		"use strict";
		
		var onResize = function() {
			if(this.resizeTimeout !== false) clearTimeout(this.resizeTimeout);
			this.resizeTimeout = setTimeout(function() {
				this.onResize();
				this.resizeTimeout = false;
			}.bind(this), 500);

			this.onResize();
		};
		
		if(window.addEventListener) {
			window.addEventListener('resize', onResize.bind(this));
		} else {
			window.attachEvent('onresize', onResize.bind(this));
		}
	},

	/**
	 * Sizes all of the elements in the NodeList "this.backgrounds"
	 */
	onResize: function() {
		"use strict";
		
		var i, node, type;

		for(i = 0; node = this.backgrounds[i]; i++) {
			if(node.type === "cover") {
				this.backgroundCover(node);
			} else {
				this.backgroundContain(node);
			}
		}
		
		console.log("onresize");
	},

	/**
	 * Sizes "node" in a way that mimicks the CSS "background-size: cover;" property setting.
	 * @param node object This should be a HTML DOM Element Object
	 */
	backgroundCover: function(node) {
		var info = this.getInfo(node).size;
		
		if(isNaN(info.width) || isNaN(info.height)) {
			throw new TypeError("node is missing require attributes 'width' or 'height'");
		}
		
		var newInfo = { };
		
		if((info.parentWidth / info.width) * info.height < info.parentHeight) {
			newInfo.height   = info.parentHeight;
			newInfo.width    = Math.ceil((info.parentHeight / info.height) * info.width);
			newInfo.left     = (info.parentWidth / 2) - (newInfo.width / 2);
			newInfo.top      = 0;
		} else {
			newInfo.height   = Math.ceil((info.parentWidth / info.width) * info.height);
			newInfo.width    = info.parentWidth;
			newInfo.left     = 0;
			newInfo.top      = (info.parentHeight / 2) - (newInfo.height / 2);
		}
		
		this.setInfo(node, newInfo);
	},

	/**
	 * Sizes "node" in a way that mimicks the CSS "background-size: contain;" property setting.
	 * @param node object This should be a HTML DOM Element Object
	 */
	backgroundContain: function(node) {
		var info = this.getInfo(node).size;

		if(isNaN(info.width) || isNaN(info.height)) {
			throw new TypeError("node is missing require attributes 'width' or 'height'");
		}
		
		var newInfo = { };

		if((info.parentWidth / info.width) * info.height < info.parentHeight) {
			newInfo.height   = Math.ceil((info.parentWidth / info.width) * info.height);
			newInfo.width    = info.parentWidth;
			newInfo.left     = 0;
			newInfo.top      = (info.parentHeight / 2) - (newInfo.height / 2);
		} else {
			newInfo.height   = info.parentHeight;
			newInfo.width    = Math.ceil((info.parentHeight / info.height) * info.width);
			newInfo.left     = (info.parentWidth / 2) - (newInfo.width / 2);
			newInfo.top      = 0;
		}

		this.setInfo(node, newInfo);
	}
};
